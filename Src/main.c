
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"

/* USER CODE BEGIN Includes */
#include <string.h>
#include <stdlib.h>
#include "usart.h"
#include "ring_buffer.h"
//Timer jest ustawiony aby wywolywal przerwanie raz na 50 sec czyli 0.02 Hz aby tak bylo nalezy prescaler ustawic na 49999 i period na 15999
//Timer na 10s czyli 1/10 Hz czyli prescaler = 19999 i period = 7999
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
TIM_HandleTypeDef htim10;
TIM_HandleTypeDef htim11;

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart3;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
/* 	phone - numer telefonu
		phonedate - AT+CMGS="+48<numer telefonu>", komenda do wysylania SMS
		gpsdana - zapis do char szerokosci geograficznej z pamieci nieulotnej
		gpsdana2 - zapis do char dlugosci geograficznej z pamieci nieulotnej
		gpsN - zapis do char szerokosci geograficznej (potrzebne do konwersji na float)
		gpsE - zapis do char dlugosci geograficznej (potrzebne do konwersji na float)
		gpsfN, gpsfE - zapis wspolrzednych geograficznych do float
		read, read2 - tymczasowe zmienne potrzebne do zapisu danych z pamieci Flash do zmiennej typu float
		Rx_data, Rxdata3 - bufory do zapisywania danych pochodzacych od UART1 i UART3
		Rx_Buffer, RxBuffer3 - bufory kolowe do przechowywania danych
		data, data2 - odczyt danych z pamieci Flash
		flag - odpowiednio ustawiana po przyjciu wiadomosci SMS oraz przy timerze
		row_UART1, row_UART3 - przechowuja caly odebrany przez UART wiersz az do znaku '\n'
		irow_UART1, irow_UART3 - aktualna dlugosc wiersza
*/
char phone[11] = "", phonedate[25] = "", gpsdana[40] = "", gpsdana2[40] = "", gpsN[12] = "", gpsE[12] = "";
char row_UART1[512] = "", row_UART3[1024] = "", temp2;
char Rx_data[2] = "", Rx_Buffer[100] = "", Rx_data3[2] = "", Rx_Buffer3[100] = "";
float gpsfN = 0, gpsfE = 0, read = 0, read2 = 0;
uint32_t data = 0, data2 = 0;
uint8_t flag = 0;
uint16_t irow_UART1 = 0, irow_UART3 = 0, i = 0;

// UART transmit buffer descriptor
static RingBuffer UART3_RingBuffer_Rx;
// UART transmit buffer memory pool
static char RingBufferData_Rx_UART3[1024];

// UART receive buffer descriptor
static RingBuffer UART1_RingBuffer_Rx;
// UART receive buffer memory pool
static char RingBufferData_Rx_UART1[1024];
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_USART3_UART_Init(void);
static void MX_TIM10_Init(void);
static void MX_TIM11_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void Flash_write(uint32_t flashAdress1, uint32_t flashAdress2, uint32_t flashData1, uint32_t flashData2);
uint32_t Flash_read(uint32_t flashAdress);

void sendSMS(char* phone)
{
	data = Flash_read(0x08004000); //odczytywanie danych z konkretnego adresu pamieci Flash
	read = *(float *)&data;
	sprintf(gpsdana, "%f\n", read);
	
	data2 = Flash_read(0x08004004);
	read2 = *(float *)&data2;
	sprintf(gpsdana2, "%f", read2);
	sprintf(phonedate, "AT+CMGS=\"+48%s\"\r\n", phone); //wysylanie SMS 
	HAL_Delay(100);
	HAL_UART_Transmit_IT(&huart3, (uint8_t *) phonedate, strlen(phonedate));
	HAL_Delay(400);
	HAL_UART_Transmit_IT(&huart3, (uint8_t *) "Brama osiedlowa\n5462\n", strlen("Brama osiedlowa\n9081\n"));
	HAL_Delay(100);
	HAL_UART_Transmit_IT(&huart3, (uint8_t *) gpsdana, strlen(gpsdana));
	HAL_Delay(10);
	HAL_UART_Transmit_IT(&huart3, (uint8_t *) gpsdana2, strlen(gpsdana));
	HAL_Delay(10);
	HAL_UART_Transmit_IT(&huart3, (uint8_t *) "\032", 1); //CTRL-Z - konczy wiadomosc SMS
	HAL_Delay(10);
}

void readFlash()
{
	HAL_UART_Transmit_IT(&huart1, (uint8_t *) "Odczytywanie danych z pamieci Flash\r\n", strlen("Odczytywanie danych z pamieci Flash\r\n"));
	HAL_Delay(10);
	
	data = Flash_read(0x08004000);
	read = *(float *)&data;
	sprintf(gpsdana, "Szerokosc geograficzna: %f\r\n", read);
	HAL_UART_Transmit_IT(&huart1, (uint8_t *) gpsdana, strlen(gpsdana));
	HAL_Delay(10);
	
	data2 = Flash_read(0x08004004);
	read2 = *(float *)&data2;
	sprintf(gpsdana2, "Dlugosc geograficzna: %f\r\n", read2);
	HAL_UART_Transmit_IT(&huart1, (uint8_t *) gpsdana2, strlen(gpsdana2));
	HAL_Delay(10);
	flag=0;
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if(htim->Instance == TIM10)// Jezeli przerwanie pochodzi od timera 10
	{ 
		HAL_GPIO_WritePin(GPIOG, GPIO_PIN_14, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GPIOG, GPIO_PIN_13, GPIO_PIN_RESET);
		HAL_TIM_Base_Stop(&htim10);
	}
 
	if(htim->Instance == TIM11) // Jezeli przerwanie pochodzi od timera 11
	{
		HAL_GPIO_WritePin(GPIOG, GPIO_PIN_2, GPIO_PIN_RESET);
		HAL_TIM_Base_Stop(&htim11);
		flag = 4;
	}
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) //funkcja ta jest wywolywana po odebraniu ilosci znakow zapisanej w HAL_UART_Receive_IT
{
	if (huart->Instance == USART1)  //przerwanie od USART1
	{	
		RingBuffer_PutChar(&UART1_RingBuffer_Rx, Rx_data[0]);
		HAL_UART_Receive_IT(&huart1, (uint8_t *) Rx_data, 1); // Ponowne wlaczenie nasluchiwania					
	}

	if (huart->Instance == USART3)  //przerwanie od USART3
	{
		RingBuffer_PutChar(&UART3_RingBuffer_Rx, Rx_data3[0]);
		HAL_UART_Receive_IT(&huart3, (uint8_t *) Rx_data3, 1); // Ponowne wlaczenie nasluchiwania
	}
}

void Flash_write(uint32_t flashAdress1, uint32_t flashAdress2, uint32_t flashData1, uint32_t flashData2)
{
	HAL_FLASH_Unlock();
	__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_PGSERR | FLASH_FLAG_PGPERR);
	FLASH_Erase_Sector(FLASH_SECTOR_1, VOLTAGE_RANGE_3); //kasowanie pamieci
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, flashAdress1, flashData1);
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, flashAdress2, flashData2);
	HAL_FLASH_Lock();
}

uint32_t Flash_read(uint32_t flashAdress)
{
	return *(uint32_t*) flashAdress;
}

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
	
  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART1_UART_Init();
  MX_USART3_UART_Init();
  MX_TIM10_Init();
  MX_TIM11_Init();
  /* USER CODE BEGIN 2 */
	HAL_UART_Receive_IT(&huart1, (uint8_t *) Rx_data, 1); //ustawienie przerwania od UARTa
	HAL_UART_Receive_IT(&huart3, (uint8_t *) Rx_data3, 1);
	
	//inicjalizacja buforow kolowych
	RingBuffer_Init(&UART3_RingBuffer_Rx, RingBufferData_Rx_UART3, sizeof(RingBufferData_Rx_UART3));
	RingBuffer_Init(&UART1_RingBuffer_Rx, RingBufferData_Rx_UART1, sizeof(RingBufferData_Rx_UART1));

  /* USER CODE END 2 */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
		if (flag == 4) //sygnalizacja za pomoca UART otwarcia bramy oraz mozliwosc obliczenia czasu otwarcia bramy
		{
			HAL_UART_Transmit_IT(&huart1, (uint8_t *) "Brama zostala zamknieta - przekaznik wylaczony\r\n", strlen("Brama zostala zamknieta - przekaznik wylaczony\r\n"));
			HAL_Delay(10);
			HAL_UART_Transmit_IT(&huart3, (uint8_t *) "AT+CCLK?\r\n", strlen("AT+CCLK?\r\n"));
			HAL_Delay(10);
			flag = 0;
		}
		if (UART1_RingBuffer_Rx.length != 0) //jesli odebrano dana z UART1
		{
			RingBuffer_GetChar(&UART1_RingBuffer_Rx, &temp2);
			row_UART1[irow_UART1] = temp2;
			irow_UART1++;
			
			if (temp2 == '\n')
			{
				if (!memcmp(row_UART1, "send", 4))
				{
					sendSMS(phone);	
				}
				else if (!memcmp(row_UART1, "read", 4))
				{
					readFlash(); //pokazuje zawartosc pamieci Flash
				}
				else
				{
					HAL_Delay(1);
					HAL_UART_Transmit_IT(&huart3, (uint8_t *) &row_UART1, strlen(row_UART1));
				}
				
				HAL_Delay(10);
				memset(row_UART1, 0, sizeof(row_UART1)); //czyszczenie pamieci
				irow_UART1 = 0;
			}
		}
		if (UART3_RingBuffer_Rx.length != 0) //jesli odebrano dana z UART3
		{
			RingBuffer_GetChar(&UART3_RingBuffer_Rx, &temp2);
			row_UART3[irow_UART3] = temp2;
			irow_UART3++;
			
			if (temp2 == '\n')
			{
				HAL_Delay(10);
				HAL_UART_Transmit_IT(&huart1, (uint8_t *) &row_UART3, strlen(row_UART3));
				HAL_Delay(5);
				
				if (!memcmp(row_UART3, "+CREG: 1\r\n", 10)) //jesli wlaczymy modul
				{
					HAL_UART_Transmit_IT(&huart1, (uint8_t *) "Ustawienie trybu odczytywania wiadomosci po wlaczeniu modulu\r\n", strlen("Ustawienie trybu odczytywania wiadomosci po wlaczeniu modulu\r\n"));
					HAL_Delay(5);
					//ustaw tryb odczytywania wiadomosci przy starcie
					HAL_UART_Transmit_IT(&huart3, (uint8_t *) "AT+CMGF=1\r\n", strlen("AT+CMGF=1\r\n"));
				}
				else if (!memcmp(row_UART3, "+CMT:", 5)) //jesli odebrano SMS
				{
					memcpy(phone, row_UART3+10, 9); //zapisz nr telefonu
					HAL_UART_Transmit_IT(&huart1, (uint8_t *) "Odebrano SMS o tresci:\r\n", strlen("Odebrano SMS o tresci:\r\n"));
					flag = 1;
				}
				else if ((!memcmp(row_UART3, "2988", 4)) && flag == 1) //wyslij wspolrzedne bramy
				{
					HAL_UART_Transmit_IT(&huart1, (uint8_t *) "Wysylanie wspolrzednych geograficznych bramy\r\n", strlen("Wysylanie wsp�lrzednych geograficznych bramy\r\n"));
					sendSMS(phone);
					flag = 0;
				}
				//administrator podaje dane wspolzedne geograficzne
				else if (((!memcmp(row_UART3, "9081", 4)) && flag == 1) || flag == 2 || flag == 3) 
				{
					if (flag == 1)
					{
						for (i=0; i<sizeof(gpsE); i++) //czyszczenie pamieci
						{
							gpsE[i] = 0;
							gpsN[i] = 0;
						}
						flag++;
					}
					else if (flag == 2)
					{
						i = 0;
						while (row_UART3[i] != 10) {	i++;}
						memcpy(gpsN, row_UART3, i); //zapisanie szerokosci geograficznej
						gpsfN = atof(gpsN);
						flag++;
					}
					else if (flag == 3)
					{
						i = 0;
						while (row_UART3[i] != 13)	{	i++;}
						memcpy(gpsE, row_UART3, i); //zapisanie dlugosci geograficznej
						gpsfE = atof(gpsE);
						
						if ( -90 <= gpsfN && gpsfN <= 90 && -180 <= gpsfE && gpsfE <= 180) //sprawdzenie czy wprowadzone dane sa prawidlowe
						{
							HAL_UART_Transmit_IT(&huart1, (uint8_t *) "Zapis wspolrzednych geograficznych do pamieci Flash\r\n", strlen("Zapis wsp�lrzednych geograficznych do pamieci Flash\r\n"));
							HAL_Delay(10);
							Flash_write(0x08004000,0x08004004, *(uint32_t *) &gpsfN, *(uint32_t *) &gpsfE);
							readFlash();
							HAL_UART_Transmit_IT(&huart1, (uint8_t *) "Potwierdzenie poprawnego zapisu danych (zapalenie zielonej diody)\r\n", strlen("Potwierdzenie poprawnego zapisu danych (zapalenie zielonej diody)\r\n"));
							HAL_Delay(10);
							HAL_TIM_Base_Start_IT(&htim10);
							HAL_GPIO_WritePin(GPIOG, GPIO_PIN_13, GPIO_PIN_SET);
						}
						else
						{
							HAL_UART_Transmit_IT(&huart1, (uint8_t *) "Podane dane sa bledne (zapalenie czerwonej diody)\r\nSzerokosc geograficzna musi zawierac sie w przedziale od -90 do 90\r\nDlugosc geograficzna musi zawierac sie w przedziale od -180 do 180\r\n", 
							strlen("Podane dane sa bledne (zapalenie czerwonej diody)\r\nSzerokosc geograficzna musi zawierac sie w przedziale od -90 do 90\r\nDlugosc geograficzna musi zawierac sie w przedziale od -180 do 180\r\n"));
							HAL_Delay(30);
							HAL_TIM_Base_Start_IT(&htim10);
							HAL_GPIO_WritePin(GPIOG, GPIO_PIN_14, GPIO_PIN_SET);
						}
						flag = 0;
					}
				}
				else if (!memcmp(row_UART3, "5462", 4) && flag == 1) //otworz brame
				{
					HAL_UART_Transmit_IT(&huart1, (uint8_t *) "Brama zostala otwarta - przekaznik wlaczony\r\n", strlen("Brama zostala otwarta - przekaznik wlaczony\r\n"));
					HAL_Delay(10);
					TIM11->CNT = 0; //bo jesli licznik jest w trakcie liczenia i ktos wysle SMS to timer zaczyna liczyc od nowa
					HAL_TIM_Base_Start_IT(&htim11);
					HAL_GPIO_WritePin(GPIOG, GPIO_PIN_2, GPIO_PIN_SET);
				}
				
				HAL_Delay(5);
				memset(row_UART3, 0, sizeof(row_UART3));
				irow_UART3 = 0;
			}
		}	
	}
		
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

  
  /* USER CODE END 3 */


}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
	
  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE3);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* TIM10 init function */
static void MX_TIM10_Init(void)
{

  htim10.Instance = TIM10;
  htim10.Init.Prescaler = 19999;
  htim10.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim10.Init.Period = 7999;
  htim10.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  if (HAL_TIM_Base_Init(&htim10) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* TIM11 init function */
static void MX_TIM11_Init(void)
{

  htim11.Instance = TIM11;
  htim11.Init.Prescaler = 49999;
  htim11.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim11.Init.Period = 15999;
  htim11.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  if (HAL_TIM_Base_Init(&htim11) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* USART1 init function */
static void MX_USART1_UART_Init(void)
{

  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

//	HAL_GPIO_EXTILineConfig(GPIO_PortSourceGPIOD, GPIO_PinSource0);
}

/* USART3 init function */
static void MX_USART3_UART_Init(void)
{

  huart3.Instance = USART3;
  huart3.Init.BaudRate = 115200;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOB_CLK_ENABLE(); //UART3
  __HAL_RCC_GPIOG_CLK_ENABLE(); 
  __HAL_RCC_GPIOA_CLK_ENABLE(); //UART1

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOG, GPIO_PIN_2|GPIO_PIN_13|GPIO_PIN_14, GPIO_PIN_RESET);

  /*Configure GPIO pins : PG2 PG13 PG14 */
  GPIO_InitStruct.Pin = GPIO_PIN_2|GPIO_PIN_13|GPIO_PIN_14;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
