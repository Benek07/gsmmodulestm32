/* Includes ------------------------------------------------------------------*/
#define NDEBUG
#include <assert.h>
#include "ring_buffer.h"

bool RingBuffer_Init(RingBuffer *ringBuffer, char *dataBuffer, size_t dataBufferSize) 
{
	assert(ringBuffer);
	assert(dataBuffer);
	assert(dataBufferSize > 0);
	
	if ((ringBuffer) && (dataBuffer) && (dataBufferSize > 0)) {
	  ringBuffer->data = dataBuffer;
		ringBuffer->head = ringBuffer->data;
		ringBuffer->tail = ringBuffer->head;
		ringBuffer->length = 0;
		ringBuffer->size = dataBufferSize;
		return true;
	}
	
	return false;
}

bool RingBuffer_Clear(RingBuffer *ringBuffer) {
	assert(ringBuffer);
	
	if (ringBuffer) {
		for(int i = 0; i < ringBuffer->size; i++) {
				ringBuffer->data[i] = 0;
		}
		ringBuffer->head = ringBuffer->data;
		ringBuffer->tail = ringBuffer->head;
		ringBuffer->length = 0;
		return true;
	}
	
	return false;
}

bool RingBuffer_IsEmpty(const RingBuffer *ringBuffer) {
  assert(ringBuffer);	
	
	if(ringBuffer && ringBuffer->length != 0) return false;
	
	return true;
}

size_t RingBuffer_GetLen(const RingBuffer *ringBuffer)
{
	assert(ringBuffer);
	
	if (ringBuffer) {
		return ringBuffer->length;
	}
	
	return 0;
}

size_t RingBuffer_GetCapacity(const RingBuffer *ringBuffer)
{
	assert(ringBuffer);
	
	if (ringBuffer) {
		return ringBuffer->size;
	}
	
	return 0;	
}


bool RingBuffer_PutChar(RingBuffer *ringBuffer, char c)
{
	assert(ringBuffer);
	
	if (ringBuffer && ringBuffer->length < ringBuffer->size) {
		ringBuffer->length++;

		if((ringBuffer->head - ringBuffer->data) == ringBuffer->size) ringBuffer->head = ringBuffer->data;
		
		*(ringBuffer->head++) = c;
		return true;
	}
	
	return false;
}

bool RingBuffer_GetChar(RingBuffer *ringBuffer, char *c)
{
	assert(ringBuffer);
	assert(c);
	
  if ((ringBuffer) && (c) && !RingBuffer_IsEmpty(ringBuffer)) {
		ringBuffer->length--;

		if((ringBuffer->tail - ringBuffer->data) == ringBuffer->size) ringBuffer->tail = ringBuffer->data;
		
		*c = *(ringBuffer->tail++);
		return true;
	}
	
	return false;
}
