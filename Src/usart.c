/* Includes ------------------------------------------------------------------*/
#include "usart.h"
#include <stdio.h>
#include <string.h>
#include "ring_buffer.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_rcc.h"
#include "stm32f4xx_hal_uart.h"
#include "stm32_hal_legacy.h"
#define NDEBUG
#include "assert.h"
#include "core.h"

/* Private definitions -------------------------------------------------------*/

/* Definition for USARTx clock resources */
#define USARTx                           USART1
#define USARTx_CLK_ENABLE()              __USART1_CLK_ENABLE();
#define USARTx_RX_GPIO_CLK_ENABLE()      __GPIOA_CLK_ENABLE()
#define USARTx_TX_GPIO_CLK_ENABLE()      __GPIOA_CLK_ENABLE() 

#define USARTx_FORCE_RESET()             __USART1_FORCE_RESET()
#define USARTx_RELEASE_RESET()           __USART1_RELEASE_RESET()

/* Definition for USARTx Pins */
#define USARTx_TX_PIN                    GPIO_PIN_9
#define USARTx_TX_GPIO_PORT              GPIOA  
#define USARTx_TX_AF                     GPIO_AF7_USART1
#define USARTx_RX_PIN                    GPIO_PIN_10
#define USARTx_RX_GPIO_PORT              GPIOA 
#define USARTx_RX_AF                     GPIO_AF7_USART1

/* Definition for USARTx's NVIC */
#define USARTx_IRQn                      USART1_IRQn
#define USARTx_IRQHandler                USART1_IRQHandler

/* Private variables ---------------------------------------------------------*/


// UART transmit buffer descriptor
static RingBuffer USART_RingBuffer_Tx;
// UART transmit buffer memory pool
static char RingBufferData_Tx[1024];

// UART receive buffer descriptor
static RingBuffer USART_RingBuffer_Rx;
// UART receive buffer memory pool
static char RingBufferData_Rx[1024];

/// UART descriptor
static UART_HandleTypeDef UartHandle;



bool USART_PutChar(char c){
		
	CORE_EnterCriticalSection();
	if(RingBuffer_PutChar(&USART_RingBuffer_Tx, c)) {
		__HAL_UART_ENABLE_IT(&UartHandle, UART_IT_TXE);
		CORE_ExitCriticalSection();
		return true;
	}
	return false;
}


size_t USART_WriteData(const void *data, size_t dataSize){
	
	size_t i;		
	for(i = 0; i < dataSize; i++) {
		if(USART_PutChar(*((const char *)(data) + i)) == false) break;
	}
	return i;
}


size_t USART_WriteString(const char *string){
	
	return USART_WriteData(string, strlen(string));
}


bool USART_GetChar(char *c){
	
	assert(c);
	CORE_EnterCriticalSection();
	if((c) && RingBuffer_GetChar(&USART_RingBuffer_Rx, c)) {
		CORE_ExitCriticalSection();
		return true;
	}
	CORE_ExitCriticalSection();
 	return false;
}


size_t USART_ReadData(char *data, size_t maxSize){
	
	size_t i;
	for(i = 0; i < maxSize; i++) {
		if(USART_GetChar(data + i) == false)
			break;
	}		
	return i;
}

bool USART_SetCallback_OnNewLine(int TODO){
	
	return false;
}

bool USART_Init(void){

	RingBuffer_Init(&USART_RingBuffer_Tx, RingBufferData_Tx, sizeof(RingBufferData_Tx));
	RingBuffer_Init(&USART_RingBuffer_Rx, RingBufferData_Rx, sizeof(RingBufferData_Rx));
	
  UartHandle.Instance          = USARTx;
  UartHandle.Init.BaudRate     = 115200;
  UartHandle.Init.WordLength   = UART_WORDLENGTH_8B;
  UartHandle.Init.StopBits     = UART_STOPBITS_1;
  UartHandle.Init.Parity       = UART_PARITY_NONE;
  UartHandle.Init.HwFlowCtl    = UART_HWCONTROL_NONE;
  UartHandle.Init.Mode         = UART_MODE_TX_RX;
  UartHandle.Init.OverSampling = UART_OVERSAMPLING_16;
    
	HAL_UART_MspInit(&UartHandle);
	
  if(HAL_UART_Init(&UartHandle) != HAL_OK)
  {
		printf("ERROR in USART_Init");
    return false;
  }

  __HAL_UART_ENABLE_IT(&UartHandle, UART_IT_RXNE); 
	
	return true;
}

